INSTALL = install -D -m 644
ECHO = echo                                                                       
NAME = calculate
DESTDIR =                                                                         
MKDIR = mkdir -p                                                                  
FIND = find 
CHMOD = chmod
COPYLINK = cp -P
LN = ln -sf

EXES = all

all:
	@${ECHO} -n

install: all
	@${FIND} install -wholename \*.svn -prune -o -type d -exec ${MKDIR} -m 755 ${DESTDIR}/usr/${NAME}/{} \;
	@${FIND} install -wholename \*.svn -prune -o -type f -exec ${INSTALL} {} ${DESTDIR}/usr/${NAME}/{} \;
	@${FIND} install -wholename \*.svn -prune -o -type l -exec ${COPYLINK} {} ${DESTDIR}/usr/${NAME}/{} \;
	@${CHMOD} 755 ${DESTDIR}/usr/${NAME}/install/calculate
	@${CHMOD} 755 ${DESTDIR}/usr/${NAME}/install/cl-unmask
	@${MKDIR} ${DESTDIR}/usr/bin
	@${LN} ${DESTDIR}/usr/calculate/install/cl-unmask ${DESTDIR}/usr/bin/cl-unmask
